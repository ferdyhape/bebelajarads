import {
    createRouter,
    createWebHistory
} from "vue-router";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import Login from "../views/Auth/Login.vue";
import Menu from "../views/Customer/Menu.vue";

const routes = [{
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/about",
        name: "About",
        component: About
    },
    {
        path: "/login",
        name: "Login",
        component: Login
    },
    {
        path: "/menu",
        name: "Menu",
        component: Menu
    }

];

const router = createRouter({
    history: createWebHistory(), // Add parentheses here
    routes // Add the routes configuration
});

export default router;
