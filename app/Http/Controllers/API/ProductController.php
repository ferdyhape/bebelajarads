<?php

namespace App\Http\Controllers\API;

use Ramsey\Uuid\Uuid;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;

class ProductController extends Controller
{
    public function index()
    {
        try {
            $products = Product::all();
            return response()->json([
                'products' => ProductResource::collection($products),
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function show(Product $product)
    {
        try {
            return response()->json([
                'product' => new ProductResource($product),
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function update($id, ProductUpdateRequest $request)
    {
        // dd($request->validated());
        try {
            DB::beginTransaction();
            $product = Product::findOrFail($id);
            $product->update($request->validated());
            if ($request->hasFile('image')) {

                // delete old image
                $product->clearMediaCollection('products');

                // upload new image
                $product->addMediaFromRequest('image')
                    ->usingName($product->name) // rename file name to product name
                    ->usingFileName(Uuid::uuid4()->toString() . '.' . $request->file('image')->getClientOriginalExtension()) // rename filename to uuid
                    ->toMediaCollection('products'); // save file to products collection
            }

            DB::commit();
            return response()->json([
                'product' => new ProductResource($product),
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function store(ProductCreateRequest $request)
    {
        try {
            $product = Product::create($request->validated());

            if ($request->hasFile('image')) {
                $product->addMediaFromRequest('image')
                    ->usingName($product->name) // rename file name to product name
                    ->usingFileName(Uuid::uuid4()->toString() . '.' . $request->file('image')->getClientOriginalExtension()) // rename filename to uuid
                    ->toMediaCollection('products'); // save file to products collection
            }

            return response()->json([
                'product' => new ProductResource($product),
            ], 201);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $product = Product::findOrFail($id);
            $product->clearMediaCollection('products');
            $product->delete();
            return response()->json([
                'message' => 'Product deleted successfully',
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
