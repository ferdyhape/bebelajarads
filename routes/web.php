<?php

use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     $products = Product::all();
//     // dd($products);
//     return view('welcome', [
//         'products' => $products,
//     ]);
// });


Route::get('/{any}', function () {
    return view('app');
})->where('any', '.*');
